// snakeService.js
async function getAll() {
  return fetchData('GET');
}

async function saveResult(data) {
  console.log(data)
  return fetchData('POST', data);
}

async function fetchData(method, body) {
  const apiUrl = 'http://localhost:3000/api/snake'; // API endpoint
  const requestOptions = {
    method: method,
    headers: {
      'Content-Type': 'application/json',
    },
    body: body ? JSON.stringify(body) : null,
  };

  try {
    const response = await fetch(apiUrl, requestOptions);
    if (!response.ok) {
      throw new Error('Network response was not ok');
    }
    const data = await response.json();
    console.log('Data from API:', data);
    return data;
  } catch (error) {
    console.error('There has been a problem with your fetch operation:', error);
    throw error;
  }
}

export default {
  getAll,
  saveResult
}