const appSettingsPath = 'appsettings.json';

// Загружаем содержимое файла appsettings.json асинхронно с использованием Fetch API
fetch(appSettingsPath)
  .then(response => {
    if (!response.ok) {
      throw new Error('Network response was not ok');
    }
    return response.json();
  })
  .then(config => {
    dbConnectionSettings = config.dbConnectionSettings;

    // Теперь у вас есть доступ к dbConnectionSettings в браузере
    console.log(dbConnectionSettings);
  })
  .catch(error => {
    console.error('There has been a problem with your fetch operation:', error);
  });

