const fetchData = import('../services/snakeService.js').then(module => module.default);


const snakeColor = 'red';
let directionX = 1;
let directionY = 0;
let currentHeadPos = {x: 0, y: 0};
let snakeSize = 1;
let snakeQueue = [];
let foodPos = { x: 0, y: 0 };
const framePauseTime = .3;
const K = 100;
let gameSpeed = () => K / framePauseTime;
const maxFoodCount = 1;
let currentFoodCount = 0;
const minFieldSize = 10;
const maxFieldSize = 20;
let currentFieldSizeValue = minFieldSize;
let currentPlayerName;
export let statistics = [

]
//Аналог main
document.addEventListener('DOMContentLoaded', async function(){ 
    document.getElementById('fieldSize').setAttribute('max', maxFieldSize);
    document.getElementById('fieldSize').setAttribute('min', minFieldSize);
    document.getElementById('fieldSize').value = currentFieldSizeValue;
    document.getElementById('fieldSizeText').innerHTML = currentFieldSizeValue;
    document.getElementById('score').innerHTML = snakeSize + 1;
    document.getElementById("fieldSize").addEventListener('input', (event) => {
        const fieldValue = event.target.value;
        document.getElementById("fieldSizeText").innerText = fieldValue;
        resizeField(fieldValue);
    });

    await loadPlayers();    
    resizeField(currentFieldSizeValue);
    generateFood();
    setInterval(moveSnake, gameSpeed());
});

function generateTableRow(playername, score) {
    const newRow = document.createElement('tr');
    newRow.innerHTML = `

        <td class="playername">${playername}</td>
        <td class="score">${score}</td>
  `;
    return newRow;
  }

  function addRowToTable(playername, score) {
    const statisticsTable = document.querySelector('.stat-body');
    const newRow = generateTableRow(playername, score);
    statisticsTable.appendChild(newRow);
  }
  

async function loadPlayers(){
    (await fetchData).getAll().then((response) => {
        response.forEach((player) => {
            statistics.push({playername: player.playername, score: player.score})
            addRowToTable(player.playername, player.score);
        })
    });
}

function updateInterface(){
    document.getElementById('score').innerHTML = snakeSize + 1;
}

function resizeField(size) {
    currentFoodCount = 0;
    const container = document.querySelector('.field');
    container.style.setProperty('--size', size);
    container.innerHTML = '';
    for (let i = 0; i < size * size; i++) {
        const cell = document.createElement('div');
        cell.className = 'cell';
        cell.setAttribute('data-index', i);
        container.appendChild(cell);
    }
    resetGame()
    generateFood();
}

function getFieldSize(){
    return parseInt(document.getElementById('fieldSizeText').innerText);
}

document.addEventListener('keydown', function(event) {
    switch(event.keyCode) {
        case 37: // Стрелка влево
        if (directionX != 1){
            directionX = -1;
            directionY = 0
        }
            console.log('Нажата стрелка влево');
            break;
        case 38: // Стрелка вверх
        if (directionY != 1){
            directionY = -1;
            directionX = 0;
        }
            console.log('Нажата стрелка вверх');
            break;
        case 39: // Стрелка вправо
        if (directionX != -1){
            directionX = 1;
            directionY = 0;
        }
            console.log('Нажата стрелка вправо');
            break;
        case 40: // Стрелка вниз
        if (directionY != -1){
            directionX = 0;
            directionY = 1;
        }
            console.log('Нажата стрелка вниз');
            break;
    }
});

function generateFood() {
    if (currentFoodCount == maxFoodCount){
        return;
    }
    
    let fieldCells = Array(Math.pow(currentFieldSizeValue, 2)).fill().map((element, index) => index + 0);
    var cells = document.querySelectorAll('.cell');
    var snakeCells = [];
    cells.forEach(function(cell) {
        if (cell.style.backgroundColor === snakeColor) {
            snakeCells.push(cell.getAttribute('data-index'));
        }
    });

    var allowedCellIndexes = fieldCells.filter(function(element) {
        return !snakeCells.includes(element);
    });
    console.log("test")
    console.log(snakeCells)
    
    let index = allowedCellIndexes[Math.floor(Math.random() * allowedCellIndexes.length)];
    foodPos = getCoordinates(index);
    console.log(foodPos);
    fillSquare(foodPos.x, foodPos.y, 'green');
    currentFoodCount++;
}

function checkCollision(x, y, array) {
    return array.some(function(element) {
        return element.x === x && element.y === y;
    });
}
function fillSquare(x, y, color) {
    const squares = document.querySelectorAll('.cell');
    const index = getIndex(y, x);
    if (index >= 0 && index < squares.length) {
        squares[index].style.backgroundColor = color;
    }
}

function getIndex(y, x) {
    const currentFieldSizeValue = getFieldSize();
    const index = y * currentFieldSizeValue + x;
    return index;
}

function getCoordinates(index) {
    const y = Math.floor(index / currentFieldSizeValue);
    const x = index % currentFieldSizeValue;
    return { x, y };
}

async function moveSnake() {
    const size = getFieldSize();
    snakeQueue.unshift({ x: currentHeadPos.x, y: currentHeadPos.y });
    if (currentHeadPos.x === foodPos.x && currentHeadPos.y === foodPos.y) {
        snakeSize++;
        if (currentFoodCount > 0) {
            currentFoodCount--;
        }

        generateFood();
    } 
    else {
        if (snakeQueue.length > snakeSize) {
            const removed = snakeQueue.pop();
            fillSquare(removed.x, removed.y, 'white');
        }
    }

    if (checkCollision(currentHeadPos.x, currentHeadPos.y, snakeQueue.slice(1))) {
        alert('Игра окончена! Счёт: ' + snakeSize);      
        try {
             // Вызываем функцию fetchData
            const roundPlayer = {playerName: currentPlayerName, score: snakeSize};
            addRowToTable(currentPlayerName, snakeSize);
            (await fetchData).saveResult(roundPlayer).then((response) => {
                statistics.push(roundPlayer);
            });
          } catch (error) {
            console.error('Произошла ошибка при получении данных из API:', error);
          }
        resetGame();
        return;
    }

    currentHeadPos.x += directionX;
    currentHeadPos.y += directionY;
    relocateSnake(size);
    fillSquare(currentHeadPos.x, currentHeadPos.y, 'red');    
    updateInterface();
}

function resetGame() {
    enterPlayerName()
    const squares = document.querySelectorAll('.cell');
    console.log(squares)
    squares.forEach(cell => {
        cell.style.backgroundColor = 'white';
    });
    directionX = 1;
    directionY = 0;
    currentHeadPos.x = 0;
    currentHeadPos.y = 0;
    snakeQueue = [];
    snakeSize = 1;
    currentFoodCount = 0;
    generateFood();
    
}

function enterPlayerName(){
    currentPlayerName = prompt('Пожалуйста, введите ваше имя:');
    // Проверьте, если пользователь ввел имя и не нажал "Отмена"
    if (currentPlayerName == null && currentPlayerName == "") {
        currentPlayerName = "Ноунейм"
    }
}

function relocateSnake(size) {
    if (currentHeadPos.x >= size) {
        currentHeadPos.x = 0;
    }

    if (currentHeadPos.x < 0) {
        currentHeadPos.x = size;
    }

    if (currentHeadPos.y >= size) {
        currentHeadPos.y = 0;
    }

    if (currentHeadPos.y < 0) {
        currentHeadPos.y = size;
    }
}
