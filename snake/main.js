import './style.css'
import javascriptLogo from './javascript.svg'
import viteLogo from '/vite.svg'
import { setupCounter } from './counter.js'

import {statistics} from './public/scripts/script'
const statisticsTableRows = statistics.map(item => {
  return `<div class="row">
            <div class="playername">${item.playername}</div>
            <div class="score">${item.score}</div>
          </div>`;
});
const statisticsTableHTML = statisticsTableRows.join('');
document.querySelector('#app').innerHTML = `
  <div class="container">
        <div class="controls">
            <input id="fieldSize" name="fieldSize" type="range"/>
            <label for="fieldSize">Размер: <span id="fieldSizeText"></span></label>
        </div>
        <div class="wrapper">
            <div class="field"></div>
            <div class="statistics">
                <h3>СТАТИСТИКА:</h3>
                <hr/>
                <table>
                <thead>
                <tк>
                  <td>Имя</td>
                  <td>Очки</td>
                </tк>
                </thead>
                <tbody class="stat-body">
                </tbody>
                </table>
                <p>Счёт: <span id="score"></span></p>
            </div>
        </div>
        
    </div>
    <footer>
        <script type="module" src="./bandle/index.js"></script>
    </footer>
`
